//
//  OptionalConvertible.swift
//  RxKit
//
//  Created by Evgeniy Abashkin on 23/05/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import Foundation

public protocol OptionalConvertible {
    associatedtype Element
    var value: Optional<Element> { get }
}

extension Optional: OptionalConvertible {
    public var value: Optional<Wrapped> {
        return self
    }
}
