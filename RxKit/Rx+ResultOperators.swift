//
//  Rx+ResultOperators.swift
//  RxKit
//
//  Created by Evgeniy Abashkin on 07/06/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import RxSwift
import RxCocoa

extension ObservableType where E: ResultConvertible {
    public func mapSuccess<R>(_ transform: @escaping (E.Success) -> R) -> RxSwift.Observable<Result<R, E.Failure>> {
        return map { $0.value.map(transform) }
    }

    public func mapFailure<R: Error>(_ transform: @escaping (E.Failure) -> R) -> RxSwift.Observable<Result<E.Success, R>> {
        return map { $0.value.mapError(transform) }
    }
}

extension PrimitiveSequenceType where TraitType == RxSwift.SingleTrait, ElementType: ResultConvertible  {
    public func mapSuccess<R>(
        _ transform: @escaping (ElementType.Success) -> R
    ) -> PrimitiveSequence<TraitType, Result<R, ElementType.Failure>> {
        return map { $0.value.map(transform) }
    }

    public func mapFailure<R>(
        _ transform: @escaping (ElementType.Failure) -> R
    ) -> PrimitiveSequence<TraitType, Result<ElementType.Success, R>> {
        return map { $0.value.mapError(transform) }
    }
}

extension SharedSequenceConvertibleType where E: ResultConvertible {
    public func mapSuccess<R>(
        _ transform: @escaping (E.Success) -> R
    ) -> RxCocoa.SharedSequence<SharingStrategy, Result<R, E.Failure>> {
        return map { $0.value.map(transform) }
    }

    public func mapFailure<R>(
        _ transform: @escaping (E.Failure) -> R
    ) -> RxCocoa.SharedSequence<SharingStrategy, Result<E.Success, R>> {
        return map { $0.value.mapError(transform) }
    }
}
