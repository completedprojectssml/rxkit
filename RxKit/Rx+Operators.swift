//
//  Rx+Operators.swift
//  Library
//
//  Created by Vladislav Sedinkin on 15/01/2019.
//

import RxSwift
import RxCocoa

// MARK: -

extension ObservableConvertibleType {
    public func asVoidDriver() -> Driver<Void> {
        return asDriver().void()
    }

    public func asDriver() -> Driver<E> {
        return asDriver { _ in .never() }
    }
}

// MARK: -

extension ObservableType where E: OptionalConvertible {
    @available(*, deprecated, renamed: "compactMap")
    public func unwrap() -> Observable<E.Element> {
        return compactMap()
    }

    public func compactMap() -> Observable<E.Element> {
        return flatMap { Observable.from(optional: $0.value) }
    }
}

extension ObservableType {
    public func void() -> Observable<Void> {
        return map { _ in }
    }

    @available(*, deprecated, renamed: "void")
    public func mapToVoid() -> Observable<Void> {
        return void()
    }

    public func map<R>(at keyPath: KeyPath<E, R>) -> Observable<R> {
        return map { $0[keyPath: keyPath] }
    }

    @available(*, deprecated, renamed: "map(at:)")
    public func mapAt<R>(_ keyPath: KeyPath<E, R>) -> Observable<R> {
        return map(at: keyPath)
    }

    public func map<R>(to value: R) -> Observable<R> {
        return map { _ in value }
    }

    @available(*, deprecated, renamed: "map(to:)")
    public func mapWith<R>(_ value: R) -> Observable<R> {
        return map(to: value)
    }

    public func filter(at keyPath: KeyPath<E, Bool>) -> Observable<E> {
        return filter { $0[keyPath: keyPath] }
    }

    @available(*, deprecated, renamed: "filter(at:)")
    public func filterAt(_ keyPath: KeyPath<E, Bool>) -> Observable<E> {
        return filter { $0[keyPath: keyPath] }
    }

    public func filterMap<R>(_ transform: @escaping (E) throws -> R?) -> Observable<R> {
        return map(transform).compactMap()
    }

    public func filterMap<R>(at keyPath: KeyPath<E, R?>) -> Observable<R> {
        return map { $0[keyPath: keyPath] }.compactMap()
    }

    @available(*, deprecated, renamed: "filterMap(at:)")
    public func filterMapAt<R>(_ keyPath: KeyPath<E, R?>) -> Observable<R> {
        return filterMap(at: keyPath)
    }

    public func flatMapEmpty<R>() -> Observable<R> {
        return flatMap { _ in Observable.empty() }
    }
}

extension ObservableType where E == Bool {
    public func filterTrue() -> Observable<E> {
        return filter { $0 }
    }

    public func filterFalse() -> Observable<E> {
        return filter { !$0 }
    }
}

// MARK: -

extension PrimitiveSequenceType where TraitType == RxSwift.SingleTrait {
    public func void() -> PrimitiveSequence<TraitType, Void> {
        return map { _ in }
    }

    @available(*, deprecated, renamed: "void")
    public func mapToVoid() -> PrimitiveSequence<TraitType, Void> {
        return void()
    }

    public func map<R>(at keyPath: KeyPath<ElementType, R>) -> PrimitiveSequence<TraitType, R> {
        return map { $0[keyPath: keyPath] }
    }

    @available(*, deprecated, renamed: "map(at:)")
    public func mapAt<R>(_ keyPath: KeyPath<ElementType, R>) -> PrimitiveSequence<TraitType, R> {
        return map(at: keyPath)
    }

    public func map<R>(to value: R) -> PrimitiveSequence<TraitType, R> {
        return map { _ in value }
    }

    @available(*, deprecated, renamed: "map(to:)")
    public func mapWith<R>(_ value: R) -> PrimitiveSequence<TraitType, R> {
        return map(to: value)
    }

    public func filter(at keyPath: KeyPath<ElementType, Bool>) -> PrimitiveSequence<MaybeTrait, ElementType> {
        return filter { $0[keyPath: keyPath] }
    }

    @available(*, deprecated, renamed: "filter(at:)")
    public func filterAt(_ keyPath: KeyPath<ElementType, Bool>) -> PrimitiveSequence<MaybeTrait, ElementType> {
        return filter(at: keyPath)
    }
}

// MARK: -

extension SharedSequenceConvertibleType where E: OptionalConvertible {
    public func compactMap() -> RxCocoa.SharedSequence<SharingStrategy, E.Element> {
        return flatMap { .from(optional: $0.value) }
    }

    @available(*, deprecated, renamed: "compactMap")
    public func unwrap() -> RxCocoa.SharedSequence<SharingStrategy, E.Element> {
        return compactMap()
    }
}

extension SharedSequenceConvertibleType {
    public func void() -> RxCocoa.SharedSequence<SharingStrategy, Void> {
        return map { _ in }
    }

    @available(*, deprecated, renamed: "void")
    public func mapToVoid() -> RxCocoa.SharedSequence<SharingStrategy, Void> {
        return void()
    }

    public func map<R>(at keyPath: KeyPath<E, R>) -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return map { $0[keyPath: keyPath] }
    }

    @available(*, deprecated, renamed: "map(at:)")
    public func mapAt<R>(_ keyPath: KeyPath<E, R>) -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return map(at: keyPath)
    }

    public func map<R>(to value: R) -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return map { _ in value }
    }

    @available(*, deprecated, renamed: "map(to:)")
    public func mapWith<R>(_ value: R) -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return map(to: value)
    }

    public func filter(at keyPath: KeyPath<E, Bool>) -> RxCocoa.SharedSequence<SharingStrategy, E> {
        return filter { $0[keyPath: keyPath] }
    }

    @available(*, deprecated, renamed: "filter(at:)")
    public func filterAt(_ keyPath: KeyPath<E, Bool>) -> RxCocoa.SharedSequence<SharingStrategy, E> {
        return filter(at: keyPath)
    }

    public func filterMap<R>(_ transform: @escaping (E) -> R?) -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return map(transform).unwrap()
    }

    public func filterMap<R>(at keyPath: KeyPath<E, R?>) -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return map { $0[keyPath: keyPath] }.unwrap()
    }

    @available(*, deprecated, renamed: "filterMap(at:)")
    public func filterMapAt<R>(_ keyPath: KeyPath<E, R?>) -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return filterMap(at: keyPath)
    }

    public func flatMapEmpty<R>() -> RxCocoa.SharedSequence<SharingStrategy, R> {
        return flatMap { _ in .empty() }
    }
}

extension SharedSequenceConvertibleType where E == Bool {
    public func filterTrue() -> RxCocoa.SharedSequence<SharingStrategy, E> {
        return filter { $0 }
    }

    public func filterFalse() -> RxCocoa.SharedSequence<SharingStrategy, E> {
        return filter { !$0 }
    }
}
