//
//  ResultConvertible.swift
//  RxKit
//
//  Created by Evgeniy Abashkin on 07/06/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import Foundation

public protocol ResultConvertible {
    associatedtype Success
    associatedtype Failure: Error
    var value: Result<Success, Failure> { get }
}

extension Result: ResultConvertible {
    public var value: Result<Success, Failure> {
        return self
    }
}
