//
//  RxKitTests.swift
//  RxKitTests
//
//  Created by Evgeniy Abashkin on 11/06/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RxBlocking
import RxTest
@testable import RxKit

class RxKitTests: XCTestCase {

    enum TestError: String, Error {
    case test
    }

    enum OtherTestError: String, Error {
        case otherTest
    }

    func testResult() {
        let success: Result<Int, TestError> = .success(1)
        let error: Result<Int, TestError> = .failure(.test)

        XCTAssertEqual(
            try! Observable.just(success)
                .mapSuccess { "\($0)" }
                .toBlocking()
                .single(),

            success.map { "\($0)" }
        )

        XCTAssertEqual(
            try! Observable.just(error)
                .mapFailure { _ in OtherTestError.otherTest }
                .toBlocking()
                .single(),

            error.mapError { _ in OtherTestError.otherTest }
        )
    }
}
